const _each = require('lodash/each')
const _chunk = require('lodash/chunk')
const path = require('path')

// PDF Template
module.exports = (data, res) => {
    let {config, baseURL, host, templateName, utils, title, doc, downloadImg} = data
    
    // Stream PDF
    doc.pipe(res)

    // Register
    doc.registerFont('txtnormal', path.join(__dirname, 'Helvetica.otf'))
    doc.registerFont('txtbold', path.join(__dirname, 'HelveticaBold.otf'))
    doc.registerFont('txtitalic', path.join(__dirname, 'HelveticaItalic.otf'))

    // vars
    let pageWidthM20 = parseInt(doc.page.width) - 20,
    pageHeightM20 = parseInt(doc.page.height) - 20,
    totalPage = 0,
    pageCount = 1

    let fakeTotalPage = data.utils.randomPDFTotalPage(doc.info.Subject, 'Pages')
    let fileSize = data.utils.randomPDFSize(doc.info.Subject)
    let uniqueCode = data.utils.generateUniqueCode(doc.info.Subject, host, templateName)
    let uniqueDate = data.utils.generateRandomFileCreationDate(doc.info.Subject)
    let information = [
        uniqueCode, 'PDF',
        fakeTotalPage,
        fileSize,
        uniqueDate
    ].join(' | ')

    // Landing Page URL
    const landingpage = utils.landingPageURL(baseURL, config, doc.info.Subject)

    // Set total page
    const setTotalPage = n => {
        totalPage = n
    }

    // Header func
    const addHeader = () => {
        doc.fontSize(9)
        // Text
        let text = doc.font('txtbold').text(`Get free access to PDF ${doc.info.Subject} at our Ebook Library`, 20, 15)
        
        // Line
        doc.moveTo(text.x, text.y + 3).lineTo(pageWidthM20, text.y + 2).stroke()
    }

    // Footer func
    const addFooter = () => {
        doc.fillColor('black').fontSize(9)

        // Right Text
        doc.font('txtnormal').text(`${pageCount}/${totalPage}`, pageWidthM20 - 15, pageHeightM20, {
            height: 10,
            width: 15
        })

        // Left text
        doc.font('txtbold').text(`PDF File: ${doc.info.Subject} - ${uniqueCode}`, 20, pageHeightM20, {
            height: 10
        })

        // Add line
        doc.moveTo(20, pageHeightM20 - 5)
        .lineTo(pageWidthM20, pageHeightM20 - 5)
        .lineWidth(1)
        .fillAndStroke('black')
        .stroke()

        // Page count increment
        pageCount++
    }

    // Add download link
    const addDownloadLink = () => {
        // Download Image
        let imgWidth = 180
        let imgHeight = 70
        let x = (doc.page.width/2)-(imgWidth/2)
        let img = doc.image(
            path.join(downloadImg, '6.png'),
            x, undefined, {
                width: imgWidth
            }
        )
        doc.link(x, doc.y - imgHeight, imgWidth, imgHeight, landingpage)
    }

    const addHeading = text => {
        doc.moveDown(10)
        .moveTo(0, doc.y)
        .lineTo(doc.page.width, doc.y)
        .lineWidth(10)
        .fillAndStroke('#FF9900')

        const x = doc.x, y = doc.y
        doc.moveDown(2)
        const x1 = doc.x, y1 = doc.y

        // Heading
        doc.font('txtbold')
        .fillColor('black')
        .fontSize(35)
        .text(text, {
            align: 'center',
            lineGap: 1
        })

        // Information
        doc.font('txtnormal')
        .fontSize(11)
        .text(information, {
            align: 'center'
        })
        doc.moveDown(2)

        doc.rect(0, y + 5, doc.page.width, doc.y - y).fill('black')

        doc.moveTo(0, doc.y)
        .lineTo(doc.page.width, doc.y)
        .lineWidth(10)
        .fillAndStroke('#FF9900')

        // Heading
        doc.font('txtbold')
        .fillColor('white')
        .fontSize(35)
        .text(text, x1, y1, {
            align: 'center',
            lineGap: 1
        })
        
        // Information
        doc.font('txtnormal')
        .fontSize(11)
        .text(information, {
            align: 'center'
        })
    }

    // Subheading
    const subHeading = (text, align = 'left') => {
        // Set color to #FF9933
        doc.fontSize(18)

        // Download Text
        doc.font('txtbold').text(text, {
            align: align
        })
    }

    // Paragraph
    const addText = (text, options = {}) => {
        let font = ('font' in options) ? options.font: 'normal'
        let continued = ('continued' in options) ? options.continued: false
        let align = ('align' in options) ? options.align: 'justify'
        let color = ('color' in options) ? options.color: 'black'

        doc
        .fillColor(color)
        .font(`txt${font}`)
        .text(text, doc.options.margin, undefined, {
            lineGap: 2,
            continued: continued,
            align: align
        })

        if (! continued) {
            doc.moveDown()
        }
    }

    // Content variation
    const addVariation = (text) => {
        text = `${doc.info.Subject} ${text}`
        let imgWidth = 120
        let imgHeight = 37
        let rightAlign = doc.page.width - doc.options.margin - imgWidth
        const y = doc.y

        doc.fillColor('black')
        .fontSize(13)
        .font('txtbold')
        .text(text.toUpperCase(), {
            width: rightAlign - 60
        })

        doc.image(path.join(downloadImg, '14.png'), rightAlign, y - imgHeight/2, {
            width: imgWidth
        })
        doc.link(rightAlign, y - imgHeight/2, imgWidth, imgHeight, landingpage)
        doc.moveDown(3)
    }



    // Setup
    //-----------------
    setTotalPage(3)


    // Page 1
    //-----------------

    // Heading
    addHeading(doc.info.Subject.toUpperCase())
    doc.moveDown(10)

    let imgWidth = 200
    doc.image(
      path.join(downloadImg, 'barcode.png'),
      (doc.page.width/2)-(imgWidth/2), undefined, {
        width: imgWidth
    })
    doc.moveDown()
    doc.moveUp(0.5)
    doc.fontSize(11)
    doc.fillColor('black')
    doc.text('COPYRIGHT 2016, ALL RIGHT RESERVED', {
        align: 'center'
    })

    // Footer
    addFooter()



    // Page 2
    //-----------------
    doc.addPage()

    // Subheading With Image
    doc.fillColor('black').fontSize(18)
    subHeading(`${doc.info.Subject}`,'left')
    doc.moveDown()

    // Text
    doc.fontSize(11)
    addText(`This ${doc.info.Subject} Pdf file begin with Intro, Brief Discussion until the Index/Glossary page, look at the table of content for additional information, if provided. It's going to discuss primarily concerning the previously mentioned topic in conjunction with much more information related to it. As per our directory, this eBook is listed as ${uniqueCode}, actually introduced on ${uniqueDate} and then take about ${fileSize} data size.`)
    addText(`We advise you to browse our wide selection of digital book in which distribute from numerous subject as well as resources presented. If you're a student, you could find wide number of textbook, academic journal, report, and so on. With regard to product buyers, you may browse for a complete product instruction manual and also guidebook and download all of them absolutely free.`)
    addText(`Take advantage of related PDF area to obtain many other related eBook for ${doc.info.Subject}, just in case you didn't find your desired topic. This section is include the most relevant and correlated subject prior to your search. With additional files and option available we expect our readers can get what they are really searching for.`)

    // Download Button
    doc.moveDown(4)
    doc.fontSize(15)
    addText('Download or Read:', {
        font: 'bold',
        align: 'center'
    })
    doc.moveUp(0.8)
    addText(`${doc.info.Subject.toUpperCase()} PDF Here!`, {
        font: 'bold',
        align: 'center'
    })
    addDownloadLink()
    doc.moveDown(4)

    // Writer
    doc.fontSize(11)
    addText(`The writers of ${doc.info.Subject} have made all reasonable attempts to offer latest and precise information and facts for the readers of this publication. The creators will not be held accountable for any unintentional flaws or omissions that may be found.`, {
        font: 'italic'
    })
    
    // Footer
    addFooter()



    // Page 3
    //-----------------
    // Add page
    doc.addPage()

    // Header
    doc.moveDown(3)

    // Heading
    doc.fillColor('black')
    subHeading(`${doc.info.Subject.toUpperCase()}`, 'center')
    doc.moveDown(2)

    // Variations
    _each(config.variations, (variation, index) => {
        addVariation(variation)
    })
    
    // Footer
    addFooter()

    // Write to stream
    doc.end()
    return
}
