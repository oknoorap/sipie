const _each = require('lodash/each')
const _chunk = require('lodash/chunk')
const path = require('path')

// PDF Template
module.exports = (data, res) => {
    let {config, baseURL, host, templateName, utils, title, doc, downloadImg} = data
    
    // Stream PDF
    doc.pipe(res)

    // Register
    doc.registerFont('txtnormal', path.join(__dirname, 'Helvetica.otf'))
    doc.registerFont('txtbold', path.join(__dirname, 'HelveticaBold.otf'))
    doc.registerFont('txtitalic', path.join(__dirname, 'HelveticaItalic.otf'))

    // vars
    let pageWidthM20 = parseInt(doc.page.width) - 20,
    pageHeightM20 = parseInt(doc.page.height) - 20,
    variations = _chunk(config.variations, 5),
    totalPage = 0,
    pageCount = 1

    let fakeTotalPage = data.utils.randomPDFTotalPage(doc.info.Subject, 'Pages')
    let fileSize = data.utils.randomPDFSize(doc.info.Subject)
    let uniqueCode = data.utils.generateUniqueCode(doc.info.Subject, host, templateName)
    let uniqueDate = data.utils.generateRandomFileCreationDate(doc.info.Subject)
    let information = [
        uniqueCode, 'PDF',
        fakeTotalPage,
        fileSize,
        uniqueDate
    ].join(' | ')

    // Landing Page URL
    const landingpage = utils.landingPageURL(baseURL, config, doc.info.Subject)

    // Set total page
    const setTotalPage = n => {
        totalPage = n
    }

    // Header func
    const addHeader = () => {
        doc.fontSize(9)
        // Text
        let text = doc.font('txtbold').text(`Get free access to PDF ${doc.info.Subject} at our Ebook Library`, 20, 15)
        
        // Line
        doc.moveTo(text.x, text.y + 3).lineTo(pageWidthM20, text.y + 2).stroke()
    }

    // Footer func
    const addFooter = () => {
        doc.fillColor('black').fontSize(9)

        // Right Text
        doc.font('txtnormal').text(`${pageCount}/${totalPage}`, pageWidthM20 - 15, pageHeightM20, {
            height: 10,
            width: 15
        })

        // Left text
        doc.font('txtbold').text(`PDF File: ${doc.info.Title}`, 20, pageHeightM20, {
            height: 10
        })

        // Add line
        doc.moveTo(20, pageHeightM20 - 5)
        .lineTo(pageWidthM20, pageHeightM20 - 5)
        .lineWidth(1)
        .fillAndStroke('black')
        .stroke()

        // Page count increment
        pageCount++
    }

    // Add download link
    const addDownloadLink = () => {
        // Download Image
        let img = doc.image(path.join(downloadImg, 'download.png'))
        
        // Download Text
        doc.fontSize(14)
        .fillColor('blue')
        .font('txtbold').text(`Download: ${doc.info.Subject.toUpperCase()} PDF`, img.x + 25, img.y - 16, {
            link: landingpage,
            underline: true
        })
    }

    const addHeading = text => {
        let pageWidth = parseInt(doc.page.width),
        halfOfPage = parseInt(pageWidth / 3)

        // Heading
        doc.font('txtbold')
        .fillColor('black')
        .fontSize(32)
        .text(text, halfOfPage, 100, {
            lineGap: 1,
            width: (pageWidth - halfOfPage) - doc.options.margin
        })

        // Information
        doc.font('txtnormal')
        .fillColor('black')
        .fontSize(12)
        let infoText = doc.text(information, halfOfPage)

        // Add line
        doc.moveTo(halfOfPage - 20, 80)
        .lineTo(halfOfPage - 20, infoText.y + 10)
        .lineWidth(8)
        .fillAndStroke('red')
    }

    // Subheading with image
    const subHeadingImg = text => {
        let imgSrc = doc.image(path.join(downloadImg, 'pdf1.png'), {
            width: 70,
            height: 70
        })

        // Set color to blue
        doc.fontSize(20).fillColor('green')

        // Download Text
        doc.font('txtbold').text(doc.info.Subject,
            imgSrc.x + 80,
            imgSrc.y - 45,
            {
                width: parseInt((doc.page.width - doc.options.margin) - (imgSrc.x + 80))
            }
        )
    }

    // Subheading
    const subHeading = text => {
        // Set color to red
        doc.fontSize(18).fillColor('red')

        // Download Text
        doc.font('txtbold').text(`Related PDFs for ${doc.info.Subject} PDF`)
    }

    // Paragraph
    const addText = (text, font = 'normal', continued = false) => {
        doc
        .fillColor('black')
        .font(`txt${font}`)
        .fontSize(11)
        .text(text, doc.options.margin, undefined, {
            lineGap: 3,
            continued: continued,
            align: 'justify'
        })

        if (! continued) {
            doc.moveDown()
        }
    }

    // Content variation
    const addVariation = (text) => {
        text = `${doc.info.Subject} ${text}`

        let rightAlign = doc.page.width - doc.options.margin - 80
        doc.fillColor('black')
        .fontSize(13)
        .font('txtbold')
        .text(text.toUpperCase(), {
            width: rightAlign - 60
        })

        let variantLink = `${baseURL} ${utils.permalink(config, text)}.pdf`
        doc.fillColor('green')
        .fontSize(11)
        .font('txtnormal')
        .text(variantLink, {
            width: rightAlign - 60,
            link: landingpage
        })

        doc.image(path.join(downloadImg, '4.png'), rightAlign, doc.y - 40, {
            width: 80
        })
        doc.link(rightAlign, doc.y - 40, 80, 20, landingpage)
        doc.moveDown(1)
    }



    // Setup
    //-----------------
    setTotalPage(3)



    // Page 1
    //-----------------

    // Heading
    addHeading(doc.info.Subject.toUpperCase())
    doc.moveDown(23)

    // Text
    addText('If you want to possess a one-stop search and find the proper manuals on your products, you can visit this website that delivers many ', 'normal', true)
    addText(`${doc.info.Subject}`, 'italic', true)
    addText('. You can get the manual you are interested in in printed form or perhaps consider it online.')
    addText('COPYRIGHT © 2016, ALL RIGHT RESERVED')

    // Footer
    addFooter()



    // Page 2
    //-----------------
    doc.addPage()

    // Subheading With Image
    subHeadingImg()
    doc.moveDown(2)

    // Text
    addText(`This type of ${doc.info.Subject} can be a very detailed document. You will must include too much info online in this document to speak what you really are trying to achieve in your reader. Actually it will be a really comprehensive document that will give you some time now to produce.If this describes the case, then you should get one of these manual will curently have enough detailed information online that is certainly typically within a handbook. Then enough is you just need to adjust the document match your business products and details. This may plan an incredibly laborious task in toa simple, simple to perform task.`)

    addText(`${doc.info.Subject} are a great way to achieve information regarding operating certain products. Many goods that you acquire are available using their instruction manuals. These user guides are clearly built to give step-by-step information about how you ought to proceed in operating certain equipments. A handbook is really a user's help guide operating the equipments. In the event youloose the best guide or perhaps the product did not provide an guide, you can easily acquire one on theweb. Search to the manual of your choosing online. Here, it is possible to make use of the various search engines to check out the available user guide and locate usually the one you'll need. On the net,it is possible to discover the manual that you need with great ease and ease.`)
    addText(`The internet has turned into a tool ideal for locating looking ${doc.info.Subject}.Also, there are lots of sites like the parts store site, A1 Appliances Sites and much more that guide while repairing this product. In addition they assist in identifying and with specific problems make the correct product parts that may resolve the situation. Most websites like wise have an advanced database, containing new economical parts for many styles of the product. But it is important to type in the model no. plus the parts number, and discover the best repair part to the product. One could also take counsel of your professional repairman, to be able to as certain the situation plus the parts which may be needed in the DIY project.`)

    // Download Button
    doc.moveDown(1)
    addDownloadLink()
    
    // Footer
    addFooter()



    // Page 2
    //-----------------
    //
    _each(variations, (item, index) => {
        doc.addPage()

        // Subheading
        subHeading()
        doc.moveDown(2)

        // Variations
        _each(item, (variation, index) => {
            addVariation(variation)
            doc.moveDown(2)
        })

        // Footer
        addFooter()
    })

    // Write to stream
    doc.end()
    return
}
