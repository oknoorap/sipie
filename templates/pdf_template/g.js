const _each = require('lodash/each')
const _chunk = require('lodash/chunk')
const _last = require('lodash/last')
const path = require('path')

// PDF Template
module.exports = (data, res) => {
    let {config, baseURL, host, templateName, utils, title, doc, downloadImg} = data
    
    // Stream PDF
    doc.pipe(res)

    // Register
    doc.registerFont('txtnormal', path.join(__dirname, 'Helvetica.otf'))
    doc.registerFont('txtbold', path.join(__dirname, 'HelveticaBold.otf'))
    doc.registerFont('txtitalic', path.join(__dirname, 'HelveticaItalic.otf'))

    // vars
    let pageWidthM20 = parseInt(doc.page.width) - 20,
    pageHeightM20 = parseInt(doc.page.height) - 20,
    variations = _chunk(config.variations, 6),
    totalPage = 0,
    pageCount = 1

    let fakeTotalPage = data.utils.randomPDFTotalPage(doc.info.Subject, 'Pages')
    let fileSize = data.utils.randomPDFSize(doc.info.Subject)
    let uniqueCode = data.utils.generateUniqueCode(doc.info.Subject, host, templateName)
    let uniqueDate = data.utils.generateRandomFileCreationDate(doc.info.Subject)
    let year = _last(uniqueDate.split(' '))
    let information = [
        uniqueCode, 'PDF',
        fakeTotalPage,
        fileSize,
        uniqueDate
    ].join(' | ')

    // Landing Page URL
    const landingpage = utils.landingPageURL(baseURL, config, doc.info.Subject)

    // Set total page
    const setTotalPage = n => {
        totalPage = n
    }

    // Header func
    const addHeader = () => {
        doc.fontSize(9)
        // Text
        let text = doc.font('txtbold').text(`Get free access to PDF ${doc.info.Subject} at our Ebook Library`, 20, 15)
        
        // Line
        doc.moveTo(text.x, text.y + 3).lineTo(pageWidthM20, text.y + 2).stroke()
    }

    // Footer func
    const addFooter = () => {
        doc.fillColor('black').fontSize(9)

        // Right Text
        doc.font('txtnormal').text(`${pageCount}/${totalPage}`, pageWidthM20 - 15, pageHeightM20, {
            height: 10,
            width: 15
        })

        // Left text
        doc.font('txtbold').text(`PDF File: ${doc.info.Subject} - ${uniqueCode}`, 20, pageHeightM20, {
            height: 10
        })

        // Add line
        doc.moveTo(20, pageHeightM20 - 5)
        .lineTo(pageWidthM20, pageHeightM20 - 5)
        .lineWidth(1)
        .fillAndStroke('black')
        .stroke()

        // Page count increment
        pageCount++
    }

    // Add download link
    const addDownloadLink = () => {
        // Download Image
        let img = doc.image(path.join(downloadImg, 'download.png'))
        
        // Download Text
        doc.fontSize(14)
        .fillColor('blue')
        .font('txtbold').text(`Download: ${doc.info.Subject.toUpperCase()} PDF`, img.x + 25, img.y - 16, {
            link: landingpage,
            underline: true
        })
    }

    const divide = parseInt(doc.page.height/2)
    const addHeading = text => {
        doc.rect(doc.options.margin, doc.options.margin, doc.page.width - (doc.options.margin*2), divide).fill('#336699')

        doc.font('txtbold')
        .fillColor('white')
        .fontSize(35)
        .text(text, doc.options.margin, divide/2, {
            width: doc.page.width - (doc.options.margin * 2),
            align: 'center'
        })

        doc.font('txtnormal')
        .fontSize(11)
        .text(information, {
            align: 'center'
        })
    }

    // Subheading
    const subHeading = (text, align = 'left') => {
        // Set color to #FF9933
        doc.fontSize(18)

        // Download Text
        doc.font('txtbold').text(text, {
            align: align
        })
    }

    // Paragraph
    const addText = (text, options = {}) => {
        let font = ('font' in options) ? options.font: 'normal'
        let continued = ('continued' in options) ? options.continued: false
        let align = ('align' in options) ? options.align: 'justify'
        let color = ('color' in options) ? options.color: 'black'
        let lineGap = ('linegap' in options) ? options.linegap: 2
        let movedown = ('movedown' in options) ? options.movedown: true

        doc
        .fillColor(color)
        .font(`txt${font}`)
        .text(text, doc.options.margin, undefined, {
            lineGap: lineGap,
            continued: continued,
            align: align
        })

        if (movedown || (movedown && ! continued)) {
            doc.moveDown()
        }
    }

    // Content variation
    const addVariation = (text) => {
        text = `${doc.info.Subject} ${text}`
        let variantLink = `${baseURL} ${utils.permalink(config, text)}.pdf`
        let imgWidth = 80
        let imgHeight = 25
        let rightAlign = doc.page.width - doc.options.margin - imgWidth
        const y = doc.y

        doc.fillColor('black')
        .fontSize(11)
        .font('txtbold')
        .text(`${doc.info.Subject} ${text}`.toUpperCase(), {
            width: rightAlign - 60
        })
        
        doc
        .fontSize(10)
        .fillColor('green')
        .font('txtnormal')
        .text(variantLink, {
            width: rightAlign - 60,
            link: landingpage
        })

        doc.image(path.join(downloadImg, '4.png'), rightAlign, y - imgHeight/2, {
            width: imgWidth
        })
        doc.link(rightAlign, y - imgHeight/2, imgWidth, imgHeight, landingpage)
        doc.moveDown(3)
    }



    // Setup
    //-----------------
    setTotalPage(2 + variations.length)


    // Page 1
    //-----------------

    // Heading
    addHeading(doc.info.Subject.toUpperCase())
    doc.moveDown(2)

    let imgWidth = 200
    doc.image(
      path.join(downloadImg, 'barcode.png'),
      (doc.page.width/2)-(imgWidth/2), divide + 20, {
        width: imgWidth
    })
    doc.fontSize(11)
    doc.fillColor('black')
    doc.text(`COPYRIGHT ${year}, ALL RIGHT RESERVED`, doc.options.margin, divide + 80, {
        width: doc.page.width - (doc.options.margin*2),
        align: 'center'
    })

    // Footer
    addFooter()



    // Page 2
    //-----------------
    doc.addPage()

    // Subheading With Image
    doc.fillColor('#336699').fontSize(18)
    subHeading(`${doc.info.Subject}`, 'left')
    doc.moveDown()

    // Text
    doc.fontSize(11)
    addText(`This type of ${doc.info.Subject} can be a very detailed document. You will must include too much info online in this document to speak what you really are trying to achieve in yourreader. Actually it will be a really comprehensive document that will give you some time now to produce.If this describes the case, then you should get one of these manual will curently have enough detailed information online that is certainly typically within a handbook. Then enough is you just need to adjust the document match your business products and details. This may plan an incredibly laborious task in toa simple, simple to perform task.`)
    addText(`${doc.info.Subject} are a great way to achieve information regarding operatingcertain products. Many goods that you acquire are available using their instruction manuals. These userguides are clearly built to give step-by-step information about how you ought to proceed in operating certain equipments. A handbook is really a user's help guide operating the equipments. In the event you loose the best guide or perhaps the product did not provide an guide, you can easily acquire one on the web. Search to the manual of your choosing online. Here, it is possible to make use of the various search engines to check out the available user guide and locate usually the one you'll need. On the net, it is possible to discover the manual that you need with great ease and ease.`)
    addText(`The internet has turned into a tool ideal for locating looking ${doc.info.Subject}.Also, there are lots of sites like the parts store site, A1 Appliances Sites and much more that guide while repairing this product. In addition they assist in identifying and with specific problems make the correct product parts that may resolve the situation. Most websites like wise have an advanced database, containing new economical parts for many styles of the product. But it is important to type in the model no. plus the parts number, and discover the best repair part to the product. One could also take counsel of your professional repairman, to be able to ascertain the situation plus the parts which may be neededin the DIY project.`)

    // Download Button
    doc.moveDown(2)
    addDownloadLink()
    doc.moveDown(4)
    
    // Footer
    addFooter()



    // Page 3
    //-----------------
    // Add page
    // Page 3 + n
    //-----------------
    _each(variations, (item, index) => {
        // Add page
        doc.addPage()
        doc.moveDown(1)

        // Heading
        doc.fillColor('red')
        subHeading(`Related PDF's for ${doc.info.Subject}`, 'left')
        doc.moveDown(2)

        // Add variation
        _each(item, (variation, index) => {
            addVariation(variation, (index === item.length - 1)? false: true)
        })
        
        // Footer
        addFooter()
    })

    // Write to stream
    doc.end()
    return
}
