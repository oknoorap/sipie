const _chunk = require('lodash/chunk'),
_each = require('lodash/each')
const path = require('path')

// PDF Template
module.exports = (data, res) => {
    let {config, baseURL, host, templateName, utils, title, doc, downloadImg} = data

    // Stream PDF
    doc.pipe(res)

    // Register
    doc.registerFont('txtnormal', path.join(__dirname, 'Helvetica.otf'))
    doc.registerFont('txtbold', path.join(__dirname, 'HelveticaBold.otf'))

    let titleVariations = utils.variationsTitle(config, doc.info.Subject)


    // vars
    let pageWidthM20 = parseInt(doc.page.width) - 20,
    pageHeightM20 = parseInt(doc.page.height) - 20,
    variations = _chunk(titleVariations, 4),
    totalPage = 0,
    pageCount = 1
    
    const landingpage = utils.landingPageURL(baseURL, config, doc.info.Subject)

    // Set total page
    const setTotalPage = n => {
        totalPage = n
    }

    // Header func
    const addHeader = () => {
        doc.fontSize(9)
        // Text
        let text = doc.font('txtbold').text(`Get free access to PDF ${doc.info.Subject} at our Ebook Library`, 20, 15)

        // Line
        doc.moveTo(text.x, text.y + 3).lineTo(pageWidthM20, text.y + 2).stroke()
    }

    // Footer func
    const addFooter = () => {
        doc.fillColor('black').fontSize(9)

        // Right Text
        doc.font('txtnormal').text(`${pageCount}/${totalPage}`, pageWidthM20 - 15, pageHeightM20, {
            height: 10,
            width: 15
        })

        // Left text
        doc.font('txtbold').text(`PDF File: ${doc.info.Subject}`, 20, pageHeightM20, {
            height: 10
        })

        // Add line
        doc.moveTo(20, pageHeightM20 - 5)
        .lineTo(pageWidthM20, pageHeightM20 - 5)
        .stroke()

        // Page count increment
        pageCount++
    }

    // Add download link
    const addDownloadLink = () => {
        let imgSrc = doc.image(path.join(downloadImg, 'download.png'))

        // Set color to blue
        doc.fontSize(11).fillColor('blue')

        // Download Text
        doc.font('txtbold').text(`Download: ${doc.info.Subject.toUpperCase()} PDF`,
            imgSrc.x + 25,
            imgSrc.y - 10,
            {
                link: landingpage,
                underline: true
            }
        )
    }

    const addHeading = text => {
        doc.font('txtbold').fontSize(25).text(text, doc.options.margin, undefined, {
            align: 'center'
        })
    }

    // Paragraph
    const addText = (text, font = 'normal', continued = false) => {
        doc.font(`txt${font}`).text(text, doc.options.margin, undefined, {
            lineGap: 3,
            continued: continued,
            align: 'justify'
        })
        if (! continued) {
            doc.moveDown()
        }
    }

    // Content variation
    const addVariation = (text, line) => {
        // Subheading
        doc.font('txtbold').fontSize(10).text('[PDF] ', doc.options.margin, undefined, {
            continued: true
        })

        doc.fontSize(13).text(text.toUpperCase(), {
            align: 'left',
            height: 5
        })

        // Link to landing page
        let variantLink = `${baseURL} ${utils.permalink(config, text)}.pdf`
        doc.font('txtnormal')
          .fontSize(10)
          .fillColor('green')
          .text(variantLink, {
              link: landingpage
          })
          .moveDown()

        // Description
        let desc = doc.fillColor('black')
        addText('If you are looking for ', 'normal', true)
        addText(`${utils.capitalize(text)} `, 'bold', true)
        addText(` our library is  free for you. We provide copy of ${doc.info.Subject} in  digital format, so the resources that you find are reliable. There are also many Ebooks of related with  this subject...`, 'normal', false)

        // Prevent draw line at the last index
        if (line) {
            doc.moveTo(desc.x, desc.y)
            .lineTo(doc.page.width - doc.options.margin, desc.y)
            .stroke()
            .moveDown(2)
        }
    }



    // Setup
    //-----------------
    setTotalPage(1 + variations.length)



    // Page 1
    //-----------------

    // Add header
    addHeader()
    doc.moveDown(3)

    // Heading
    addHeading(`${doc.info.Subject.toUpperCase()} PDF`)
    doc.moveDown()

    // Download Image 1
    addDownloadLink()
    doc.moveDown(2)

    // Paragraph 1
    doc.fillColor('black')
    addText(`${doc.info.Subject.toUpperCase()} `, 'bold', true)
    addText(`- Are you looking for Ebook ${doc.info.Subject}? You will be glad to know that right now ${doc.info.Subject} is available on our online library. With our online resources, you can find ${doc.info.Subject} or just about any type of  ebooks, for any type of product.`)
    doc.moveDown()

    // Paragraph 2
    addText(`Best of all, they are entirely free to find, use and download, so there is no cost or stress at all. ${doc.info.Subject} may not make exciting  reading, but ${doc.info.Subject} is packed with valuable instructions, information and warnings. We also have many ebooks and user guide is also  related with ${doc.info.Subject} and many other ebooks.`)
    doc.moveDown()

    // Paragraph 3
    addText(`We have made it easy for you to find a PDF Ebooks without any digging. And by having access to  our ebooks online or by storing it on your computer, you have convenient answers with ${doc.info.Subject}. To get started finding ${doc.info.Subject}, you are right to find our website  which has a comprehensive collection of manuals listed.`)
    doc.moveDown()

    // Paragraph 4
    addText(`Our library is the biggest of these that have literally hundreds of thousands of different products  represented. You will also see that there are specific sites catered to different product types or  categories, brands or niches related with ${doc.info.Subject}. So depending on what exactly you are searching, you will be able to choose  ebooks to suit your own needs.`)
    doc.moveDown()

    // Download Image 2
    addDownloadLink()

    // Footer
    addFooter()

    // Page 2 + n
    //-----------------
    _each(variations, (item, index) => {
        // Add page
        doc.addPage()

        // Header
        addHeader()
        doc.moveDown(3)

        // Heading
        addHeading(`${doc.info.Subject.toUpperCase()} PDF`)
        doc.moveDown()

        // Add variation
        _each(item, (variation, index) => {
            addVariation(variation, (index === item.length - 1)? false: true)
        })

        // Footer
        addFooter()
    })

    // Write to stream
    doc.end()
    return
}
