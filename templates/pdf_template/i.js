const _each = require('lodash/each')
const _chunk = require('lodash/chunk')
const _last = require('lodash/last')
const path = require('path')

// PDF Template
module.exports = (data, res) => {
    let {config, baseURL, host, templateName, utils, title, doc, downloadImg} = data
    // Stream PDF
    doc.pipe(res)

    // Register
    doc.registerFont('txtnormal', path.join(__dirname, 'Helvetica.otf'))
    doc.registerFont('txtbold', path.join(__dirname, 'HelveticaBold.otf'))
    doc.registerFont('txtitalic', path.join(__dirname, 'HelveticaItalic.otf'))

    // vars
    let pageWidthM20 = parseInt(doc.page.width) - 20,
    pageHeightM20 = parseInt(doc.page.height) - 20,
    variations = _chunk(config.variations, 5),
    totalPage = 0,
    pageCount = 1

    let fakeTotalPage = data.utils.randomPDFTotalPage(doc.info.Subject, 'Pages')
    let fileSize = data.utils.randomPDFSize(doc.info.Subject)
    let uniqueCode = data.utils.generateUniqueCode(doc.info.Subject, host, templateName)
    let uniqueDate = data.utils.generateRandomFileCreationDate(doc.info.Subject)
    let year = _last(uniqueDate.split(' '))
    let information = [
        uniqueCode, 'PDF',
        fakeTotalPage,
        fileSize,
        uniqueDate
    ].join(' | ')

    // Landing Page URL
    const landingpage = utils.landingPageURL(baseURL, config, doc.info.Subject)

    // Set total page
    const setTotalPage = n => {
        totalPage = n
    }

    // Header func
    const addHeader = () => {
        doc.fontSize(9)
        // Text
        let text = doc.font('txtbold').text(`Get free access to PDF ${doc.info.Subject} at our Ebook Library`, 20, 15)

        // Line
        doc.moveTo(text.x, text.y + 3).lineTo(pageWidthM20, text.y + 2).stroke()
    }

    // Footer func
    const addFooter = () => {
        doc.fillColor('black').fontSize(9)

        // Right Text
        doc.font('txtnormal').text(`${pageCount}/${totalPage}`, pageWidthM20 - 15, pageHeightM20, {
            height: 10,
            width: 15
        })

        // Left text
        doc.font('txtbold').text(`PDF File: ${doc.info.Subject} - ${uniqueCode}`, 20, pageHeightM20, {
            height: 10
        })

        // Add line
        doc.moveTo(20, pageHeightM20 - 5)
        .lineTo(pageWidthM20, pageHeightM20 - 5)
        .lineWidth(1)
        .fillAndStroke('black')
        .stroke()

        // Page count increment
        pageCount++
    }

    // Add download link
    const addDownloadLink = () => {
        // Download Image
        let img = doc.image(path.join(downloadImg, 'download.png'))

        // Download Text
        doc.fontSize(14)
        .fillColor('blue')
        .font('txtbold').text(`Download: ${doc.info.Subject.toUpperCase()} PDF`, img.x + 25, img.y - 16, {
            link: landingpage,
            underline: true
        })
    }

    const divide = parseInt(doc.page.height/2)
    const addHeading = text => {
        // Heading
        doc.font('txtbold')
        .fontSize(35)
        .text(text, doc.options.margin, 100, {
            align: 'center',
            lineGap: 1
        })

        // Information
        doc.font('txtnormal')
        .fontSize(11)
        .text(information, {
            align: 'center'
        })
    }

    // Subheading
    const subHeading = (text, align = 'left') => {
        // Set color to #FF9933
        doc.fontSize(18)

        // Download Text
        doc.font('txtbold').text(text, {
            align: align
        })
    }

    // Paragraph
    const addText = (text, options = {}) => {
        let font = ('font' in options) ? options.font: 'normal'
        let continued = ('continued' in options) ? options.continued: false
        let align = ('align' in options) ? options.align: 'justify'
        let color = ('color' in options) ? options.color: 'black'
        let lineGap = ('linegap' in options) ? options.linegap: 2
        let movedown = ('movedown' in options) ? options.movedown: true

        doc
        .fillColor(color)
        .font(`txt${font}`)
        .text(text, doc.options.margin, undefined, {
            lineGap: lineGap,
            continued: continued,
            align: align
        })

        if (movedown || (movedown && ! continued)) {
            doc.moveDown()
        }
    }

     // Content variation
    const addVariation = (text, line) => {
        text = `${doc.info.Subject} ${text}`

        // Subheading
        doc.font('txtbold').fontSize(10).text('[PDF] ', doc.options.margin, undefined, {
            continued: true
        })

        doc.fontSize(13).text(text.toUpperCase(), {
            align: 'left',
            height: 5
        })

        // Link to landing page
        let variantLink = `${baseURL} ${utils.permalink(config, text)}.pdf`
        doc.font('txtnormal')
          .fontSize(10)
          .fillColor('green')
          .text(variantLink, {
            link: landingpage
          })
          .moveDown()

        // Description
        let desc = doc.fillColor('black')
        addText('If you are looking for ', {
            font: 'normal',
            continued: true,
            movedown: false
        })
        addText(`${utils.capitalize(text)} `, {
            font: 'bold',
            continued: true,
            movedown: false
        })
        addText(` our library is  free for you. We provide copy of ${doc.info.Subject} in  digital format, so the resources that you find are reliable. There are also many Ebooks of related with  this subject...`, {
            font: 'normal'
        })

        // Prevent draw line at the last index
        if (line) {
            doc.moveTo(desc.x, desc.y)
            .lineTo(doc.page.width - doc.options.margin, desc.y)
            .stroke()
            .moveDown(2)
        }
    }



    // Setup
    //-----------------
    setTotalPage(2 + variations.length)


    // Page 1
    //-----------------

    // Heading
    doc.moveDown(20)
    addHeading(doc.info.Subject.toUpperCase())
    doc.moveDown(5)

    let imgWidth = 200
    doc.image(path.join(downloadImg, 'barcode.png'), (doc.page.width/2)-(imgWidth/2), undefined, {
        width: imgWidth
    })
    doc.fontSize(11)
    doc.fillColor('black')
    doc.text(`COPYRIGHT ${year}, ALL RIGHT RESERVED`, {
        align: 'center'
    })

    // Footer
    addFooter()



    // Page 2
    //-----------------
    doc.addPage()

    // Subheading With Image
    doc.fillColor('black').fontSize(18)
    subHeading(`${doc.info.Subject}`, 'center')
    doc.moveDown()

    // Text
    doc.fontSize(11)
    addText(`PDF Subject: ${doc.info.Subject} Its strongly recommended to start read the Intro section, next on the Quick Discussion and find out all the topic coverage within this PDF file one after the other. Or perhaps in case you already know a precise topic, you should use the Glossary page to easily find the area of interest you are interested in, since it manage alphabetically. According to our listing, the following PDF file is submitted in ${uniqueDate}, documented in serial number of ${uniqueCode}, with data size around ${fileSize}, in case you want to download it and study it offline.`)
    addText(`We suggest you to search our broad selection of eBook in which distribute from numerous subject as well as topics accessible. If you are a college student, you can find huge number of textbook, paper, report, etc. Intended for product end-users, you may surf for a whole product manual as well as handbook and download them for free.`)
    addText(`Below, we also supply a list of some of the most related as well as relevant pdf tightly associated to your search subject of ${doc.info.Subject}. This section was established to give you the optimum result plus much more quantity of connected subjects related to your desirable topics, in which we hope could be very helpful for our readers.`)
    doc.moveDown(1)
    addText(`Download full version PDF for ${doc.info.Subject} using the link below:`)

    // Download Button
    doc.moveDown(2)
    addDownloadLink()
    doc.moveDown(4)

    doc.fontSize(11)
    addText(`The writers of ${doc.info.Subject} have made all reasonable attempts to offer latest and precise information and facts for the readers of this publication. The creators will not be held accountable for any unintentional flaws or omissions that may be found.`, {
        font: 'italic'
    })

    // Footer
    addFooter()



    // Page 3
    //-----------------
    // Add page
    // Page 3 + n
    //-----------------
    _each(variations, (item, index) => {
        // Add page
        doc.addPage()
        doc.moveDown(3)

        // Heading
        subHeading(`${doc.info.Subject.toUpperCase()} PDF`, 'center')
        doc.moveDown(2)

        // Add variation
        _each(item, (variation, index) => {
            addVariation(variation, (index === item.length - 1)? false: true)
        })

        // Footer
        addFooter()
    })

    // Write to stream
    doc.end()
    return
}