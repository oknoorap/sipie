const _each = require('lodash/each')
const _chunk = require('lodash/chunk')
const path = require('path')

// PDF Template
module.exports = (data, res) => {
    let {config, baseURL, host, templateName, utils, title, doc, downloadImg} = data
    
    // Stream PDF
    doc.pipe(res)

    // Register
    doc.registerFont('txtnormal', path.join(__dirname, 'Helvetica.otf'))
    doc.registerFont('txtbold', path.join(__dirname, 'HelveticaBold.otf'))
    doc.registerFont('txtitalic', path.join(__dirname, 'HelveticaItalic.otf'))

    // vars
    let pageWidthM20 = parseInt(doc.page.width) - 20,
    pageHeightM20 = parseInt(doc.page.height) - 20,
    variations = _chunk(config.variations, 6),
    totalPage = 0,
    pageCount = 1

    let fakeTotalPage = data.utils.randomPDFTotalPage(doc.info.Subject, 'Pages')
    let fileSize = data.utils.randomPDFSize(doc.info.Subject)
    let uniqueCode = data.utils.generateUniqueCode(doc.info.Subject, host, templateName)
    let uniqueDate = data.utils.generateRandomFileCreationDate(doc.info.Subject)
    let information = [
        uniqueCode, 'PDF',
        fakeTotalPage,
        fileSize,
        uniqueDate
    ].join(' | ')

    // Landing Page URL
    const landingpage = utils.landingPageURL(baseURL, config, doc.info.Subject)

    // Set total page
    const setTotalPage = n => {
        totalPage = n
    }

    // Header func
    const addHeader = () => {
        doc.fontSize(9)
        // Text
        let text = doc.font('txtbold').text(`Get free access to PDF ${doc.info.Subject} at our Ebook Library`, 20, 15)
        
        // Line
        doc.moveTo(text.x, text.y + 3).lineTo(pageWidthM20, text.y + 2).stroke()
    }

    // Footer func
    const addFooter = () => {
        doc.fillColor('black').fontSize(9)

        // Right Text
        doc.font('txtnormal').text(`${pageCount}/${totalPage}`, pageWidthM20 - 15, pageHeightM20, {
            height: 10,
            width: 15
        })

        // Left text
        doc.font('txtbold').text(`PDF File: ${doc.info.Subject} - ${uniqueCode}`, 20, pageHeightM20, {
            height: 10
        })

        // Add line
        doc.moveTo(20, pageHeightM20 - 5)
        .lineTo(pageWidthM20, pageHeightM20 - 5)
        .lineWidth(1)
        .fillAndStroke('black')
        .stroke()

        // Page count increment
        pageCount++
    }

    // Add download link
    const addDownloadLink = () => {
        // Download Image
        let img = doc.image(path.join(downloadImg, 'download.png'))
        
        // Download Text
        doc.fontSize(14)
        .fillColor('blue')
        .font('txtbold').text(`Download: ${doc.info.Subject.toUpperCase()} PDF`, img.x + 25, img.y - 16, {
            link: landingpage,
            underline: true
        })
    }

    const triad = parseInt(doc.page.height/3)
    const addHeading = text => {
        doc.rect(0, 0, doc.page.width, triad).fill('red')
        doc.rect(0, triad, doc.page.width, triad).fill('black')

        doc.font('txtbold')
        .fillColor('white')
        .fontSize(35)
        .text(text, doc.options.margin, triad/3, {
            width: doc.page.width - (doc.options.margin * 2),
            align: 'center'
        })

        doc.font('txtnormal')
        .fontSize(11)
        .text(information, {
            align: 'center'
        })
    }

    // Subheading
    const subHeading = (text, align = 'left') => {
        // Set color to #FF9933
        doc.fontSize(18)

        // Download Text
        doc.font('txtbold').text(text, {
            align: align
        })
    }

    // Paragraph
    const addText = (text, options = {}) => {
        let font = ('font' in options) ? options.font: 'normal'
        let continued = ('continued' in options) ? options.continued: false
        let align = ('align' in options) ? options.align: 'justify'
        let color = ('color' in options) ? options.color: 'black'
        let lineGap = ('linegap' in options) ? options.linegap: 2
        let movedown = ('movedown' in options) ? options.movedown: true

        doc
        .fillColor(color)
        .font(`txt${font}`)
        .text(text, doc.options.margin, undefined, {
            lineGap: lineGap,
            continued: continued,
            align: align
        })

        if (movedown || (movedown && ! continued)) {
            doc.moveDown()
        }
    }

    // Content variation
    const addVariation = (text) => {
        text = `${doc.info.Subject} ${text}`
        let variantLink = `${baseURL} ${utils.permalink(config, text)}.pdf`
        let imgWidth = 35
        let imgHeight = 35
        let rightAlign = doc.page.width - doc.options.margin - imgWidth
        const y = doc.y

        doc.fillColor('black')
        .fontSize(11)
        .font('txtbold')
        .text(text.toUpperCase(), {
            width: rightAlign - 60
        })
        
        doc
        .fontSize(10)
        .fillColor('green')
        .font('txtnormal')
        .text(variantLink, {
            width: rightAlign - 60,
            link: landingpage
        })

        doc.image(path.join(downloadImg, 'download2.png'), rightAlign, y - imgHeight/2, {
            width: imgWidth
        })
        doc.link(rightAlign, y - imgHeight/2, imgWidth, imgHeight, landingpage)
        doc.moveDown(3)
    }



    // Setup
    //-----------------
    setTotalPage(2 + variations.length)


    // Page 1
    //-----------------

    // Heading
    addHeading(doc.info.Subject.toUpperCase())

    doc.fontSize(15)
    doc.font('txtbold').text('TABLE OF CONTENT', doc.options.margin, triad + 50, {
        width: doc.page.width - (doc.options.margin*2),
        align: 'center',
        lineGap: 5
    })
    doc.fontSize(13)
    addText('Introduction', {
        color: 'white',
        align: 'center',
        linegap: 0,
        movedown: false
    })
    addText('Brief Description', {
        color: 'white',
        align: 'center',
        linegap: 0,
        movedown: false
    })
    addText('Main Topic', {
        color: 'white',
        align: 'center',
        linegap: 0,
        movedown: false
    })
    addText('Technical Note', {
        color: 'white',
        align: 'center',
        linegap: 0,
        movedown: false
    })
    addText('Appendix', {
        color: 'white',
        align: 'center',
        linegap: 0,
        movedown: false
    })
    addText('Glossary', {
        color: 'white',
        align: 'center',
        linegap: 0,
        movedown: false
    })
    doc.moveDown(2)

    let imgWidth = 200
    doc.image(
      path.join(downloadImg, 'barcode.png'),
      (doc.page.width/2)-(imgWidth/2), (triad*2) + 20, {
        width: imgWidth
    })
    doc.fontSize(11)
    doc.fillColor('black')
    doc.text('COPYRIGHT 2016, ALL RIGHT RESERVED', doc.options.margin, (triad*2) + 80, {
        width: doc.page.width - (doc.options.margin*2),
        align: 'center'
    })

    // Footer
    addFooter()



    // Page 2
    //-----------------
    doc.addPage()

    // Subheading With Image
    doc.fillColor('red').fontSize(18)
    subHeading(`${doc.info.Subject}`, 'left')
    doc.moveDown()

    addText('INTRODUCTION', {
        font: 'bold',
        linegap: 3
    })

    // Text
    doc.fontSize(11)
    addText(`This particular ${doc.info.Subject} PDF start with Introduction, Brief Session till the Index/Glossary page, look at the table of content for additional information, when presented. It's going to focus on mostly about the above subject together with additional information associated with it. Based on our directory, the following eBook is listed as ${uniqueCode}, actually published on ${uniqueDate} and thus take about ${fileSize} data sizing.`)
    addText(`If you are interesting in different niche as well as subject, you may surf our wonderful selection of our electronic book collection which is incorporate numerous choice, for example university or college textbook as well as journal for college student as well as virtually all type of product owners manual meant for product owner who's in search of online copy of their manual guide. You may use the related PDF section to find much more eBook listing and selection obtainable in addition to your wanting PDF of ${doc.info.Subject}.`)
    addText(`This is committed to provide the most applicable as well as related pdf within our data bank on your desirable subject. By delivering much bigger alternative we believe that our readers can find the proper eBook they require.`)
    addText(`Download full version PDF for ${doc.info.Subject} using the link below:`)

    // Download Button
    doc.moveDown(2)
    addDownloadLink()
    doc.moveDown(4)

    // Writer
    doc.fontSize(11)
    addText(`The writers of ${doc.info.Subject} have made all reasonable attempts to offer latest and precise information and facts for the readers of this publication. The creators will not be held accountable for any unintentional flaws or omissions that may be found.`, {
        font: 'italic'
    })
    
    // Footer
    addFooter()



    // Page 3
    //-----------------
    // Add page
    // Page 3 + n
    //-----------------
    _each(variations, (item, index) => {
        // Add page
        doc.addPage()
        doc.moveDown(1)

        // Heading
        doc.fillColor('black')
        subHeading(`Related PDF's for ${doc.info.Subject}`, 'center')
        doc.moveDown(2)

        // Add variation
        _each(item, (variation, index) => {
            addVariation(variation, (index === item.length - 1)? false: true)
        })
        
        // Footer
        addFooter()
    })

    // Write to stream
    doc.end()
    return
}
