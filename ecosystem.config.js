let applicationList = []

for (let i = 1; i <= 5; i++) {
  applicationList.push({
    name: `app${i}`,
    watch: true,
    script: "index.js",
    max_memory_restart: "300M",
    ignore_watch: ["node_modules", "config"],
    autorestart: true,
    env: {
      NODE_PORT: (3000 + i).toString()
    }
  })
}

module.exports = {
  apps : applicationList
}