// Important modules
const toobusy = require('toobusy-js')
const express = require('express')
const PDFDocument = require('pdfkit')
const path = require('path')
const app = express()
const fs = require('fs')
const utils = require('./utils.js')
const sitemapRgx = /sitemap(\_[a-z])?\.xml/
const cons = require('consolidate')
const ejs = require('ejs')
const _ = require('lodash')
const S = require('string')
const titleize = require('titleize')
const winston = require('winston')

// set port
const port = process.env.NODE_PORT
    // Path
let templateDir = path.join(__dirname, 'templates'),
    downloadImg = path.join(templateDir, 'download_img'),
    publicDir = path.join(templateDir, 'public')
    // set app port
app.set('port', port)
    // use template engine
app.engine('ejs', cons.ejs)
app.set('view engine', 'ejs')
app.set('views', templateDir)
    // static files
app.use(express.static(publicDir))
// Prevent Server Down
app.use(function(req, res, next) {
    if (toobusy()) {
        let hostname = (req.headers.host.match(/:/g)) ? req.headers.host.slice(0, req.headers.host.indexOf(':')) : req.headers.host
        let config = utils.domainConfig(hostname)
        let title = config.title
        let slug = S(title).slugify().s
        let baseURL = utils.baseURL(req)
        let route = req.url
        let routeSplit = route.split('/')
        if (routeSplit[1] === config.permalink.view && routeSplit[2] === config.permalink.download) {
            title = _.first(routeSplit[3].split('.'))
            title = S(title).humanize().s
            slug = S(title).slugify().s
        }
        let redirURL = `${baseURL}/?redir=${slug}`

        let data = {
            config,
            utils,
            title,
            baseURL,
            redirURL
        }
        let landingPage = fs.readFileSync(path.join(templateDir, 'misc_template', 'landing.ejs'), 'ascii')
        return res.status(503).end(ejs.render(landingPage, data))
    }
    return next()
})
    // Routers
app.get('*', (req, res, next) => {
    // check if method is get or not
    if (req.method !== 'GET') {
        return next()
    }

    let route = req.params[0],
        routeSplit = route.split('/'),
        hostname = (req.headers.host.match(/:/g)) ? req.headers.host.slice(0, req.headers.host.indexOf(':')) : req.headers.host

    let baseURL = utils.baseURL(req)
    let config = utils.domainConfig(hostname)

    // If home
    if (route === '/') {
        if (req.query.redir) {
            let title = titleize(req.query.redir)
            res.redirect(301, utils.parseLandingURL(config, config.cpa.landing_url, title))
        }

        let {rand_size, filename} = config.keyword

        // read keyword by its size and filename
        utils.readKeyword(rand_size, filename, (err, keywords) => {
            if (err) {
                return next(err)
            }

            // create data for home
            let data = {config, keywords, utils, host: hostname}

            // render by templatename
            return res.render(path.join(publicDir, config.template), data)
        })
        return
    }
    
    // for landingpage url goes to here dude
    if (routeSplit[1] === config.permalink.download) {
        let slug = path.basename(routeSplit[2], '.pdf')
        let title = titleize(S(slug).humanize().s)
        let redirURL = `${baseURL}/?redir=${slug}`
        let data = {
            config,
            utils,
            redirURL,
            title
        }

        let countdownPage = fs.readFileSync(path.join(templateDir, 'misc_template', 'countdown.ejs'), 'ascii')
        return res.send(ejs.render(countdownPage, data))
    }

    // static page begin here
    if (routeSplit[1].match(/^(privacy|dmca|contact)\.html/ig)) {
        let data = {
            config,
            utils,
            baseURL,
            year: new Date().getFullYear()
        }
        let templateName = routeSplit[1].replace(/\.html$/ig, '')
        let pageTemplate = fs.readFileSync(path.join(templateDir, 'misc_template', `${templateName}.ejs`))
        let templateString = ejs.render(pageTemplate.toString(), data)

        // set content type
        res.type('html')
        return res.status(200).send(templateString)
    }

    // robots txt
    if (routeSplit[1].match(/^robots\.txt$/ig)) {
        let robotsTemplate = fs.readFileSync(path.join(templateDir, 'misc_template', 'robots.ejs'))
        let templateString = ejs.render(robotsTemplate.toString(), {config})

        // set content type
        res.type('text')
        return res.status(200).send(templateString)
    }

    // Sitemap router begin here
    let sitemapPattern = /(index-pages|index-files|page\-(.*?)|file\-(.*?))\.xml/gi
    let matchSitemapPattern = sitemapPattern.test(routeSplit[2])
    if (routeSplit[1] === 'sitemap' && matchSitemapPattern) {
        let sitemapTemplate = fs.readFileSync(path.join(templateDir, 'misc_template', 'sitemap.ejs'))
        let alphabet = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        let alphabetMap = _.chain(alphabet).lowerCase().split('').compact().filter(data => data != ' ')
        let data = {config}
        let templateString
        let baseURL = req.headers.host

        // set content type and status
        res.type('xml')

        // sitemap for pages
        if (routeSplit[2] === 'index-pages.xml') {
            data.urls = alphabetMap.map(data => `http://${baseURL}/sitemap/page-${data}.xml`).value()
            templateString = ejs.render(sitemapTemplate.toString(), data)
            return res.status(200).send(templateString)
        }

        // sitemap for files
        if (routeSplit[2] === 'index-files.xml') {
            data.urls = alphabetMap.map(data => `http://${baseURL}/sitemap/file-${data}.xml`).value()
            templateString = ejs.render(sitemapTemplate.toString(), data)
            return res.status(200).send(templateString)
        }

        let isMatchPagePattern = routeSplit[2].match(/^page\-([a-zA-Z0-9]+)\.xml/)
        let isMatchFilePattern = routeSplit[2].match(/^file\-([a-zA-Z0-9]+)\.xml/)
        if (isMatchPagePattern || isMatchFilePattern) {
            let startingAlphabet = (isMatchPagePattern) ? isMatchPagePattern[1] : isMatchFilePattern[1]
            let extensions = (isMatchPagePattern) ? '.html' : '.pdf'

            utils.listKeywordByAlphabet(startingAlphabet, config.keyword.filename, (err, keywords) => {
                if (err) return next()

                data.urls = _.chain(keywords).compact().filter(data => data != ' ').map(data => utils.permalinkInSitemap(config, startingAlphabet, data, extensions))
                  .map(data => `http://${baseURL}${data}`).value()

                let templateString = ejs.render(sitemapTemplate.toString(), data)
                return res.status(200).send(templateString)
            })
            return
        }
        return next()
    }

    // check if  Sitemap
    if (sitemapRgx.test(route)) {
        //let sitemapType = route.match(sitemapRgx)
        /*if (sitemapType[1] !== undefined) {
            let alphabet = sitemapType[1].replace('_', '')
        }*/
        return res.send('sitemap')
    }

    // if
    if (routeSplit.length > 3) {
        let pdfPermalinkMatch = (
          (routeSplit[1] === config.permalink.view) && (routeSplit[2] === config.permalink.download))
        let pdfFromSitemapPattern = new RegExp(`\/${config.permalink.view}\/([a-zA-Z0-9]+)\/(.*?)\.(pdf|html)$`, 'gi')

        // if permalink match
        if (pdfPermalinkMatch || pdfFromSitemapPattern.test(route)) {
            let base = path.basename(route)
            let ext = path.extname(base)
            // pdf template directory
            const pdfTemplate = require(path.join(templateDir, 'pdf_template'))
            let slug = path.basename(base, ext)
            slug = S(slug).latinise().s
            let templateName = utils.randomPDFTemplate(slug)
            let renderer = pdfTemplate[templateName]
            let subject = titleize(S(slug).humanize().s)
            let uniqueCode = utils.generateUniqueCode(subject, hostname, templateName)
            let title = `${subject} ${uniqueCode}`.toUpperCase()
            let fakeAuthor = utils.generateFakeName(subject)

            let data = {
                config,
                host: req.hostname,
                templateName,
                utils,
                baseURL,
                title,
                downloadImg
            }

            if (path.extname(base) === '.pdf') {
                // write res
                res.writeHead(200, {
                    'Content-Type': 'application/pdf',
                    'Content-Disposition': `inline; filename=${base}`
                })

                // PDF Information
                let doc = new PDFDocument({
                    size: 'A4',
                    margin: 50,
                    info: {
                        Title: title,
                        Author: fakeAuthor,
                        Subject: subject,
                        Keywords: ''
                    }
                })

                data.doc = doc

                return renderer(data, res)
            }

            data.fakeTotalPage = data.utils.randomPDFTotalPage(subject, 'Pages')
            data.fileSize = data.utils.randomPDFSize(subject)
            data.uniqueCode = data.utils.generateUniqueCode(subject, req.hostname, templateName)
            data.uniqueDate = data.utils.generateRandomFileCreationDate(subject)
            data.config = config
            data.baseURL = baseURL
            data.subject = subject
            data.fakeAuthor = fakeAuthor

            // render by templatename
            return res.render(path.join(publicDir, config.template, 'page'), data)
        }
        return next()
    }

    // if not found
    return next()
})


// default 404
app.use(function(req, res, next) {
    let countdownPage = fs.readFileSync(path.join(templateDir, 'misc_template', 'not_found.ejs'), 'ascii')
    res.status(404).end(ejs.render(countdownPage))
    return next()
})
// default error handler
app.use(function(err, req, res, next) {
    winston.error(err)
    res.status(500).end('Something broke!')
    return next()
})

let server = app.listen(port, () => {
    winston.info(`SiPie listening on port ${port}!`)
})

// Graceful shutdown
process.on('SIGINT', () => {
    server.close()
    toobusy.shutdown()
    process.exit()
})
