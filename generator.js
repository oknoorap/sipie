const fs = require('fs')
const path = require('path')
const program = require('commander')
const async = require('async')
const ejs = require('ejs')
const mkdir = require('mkdirp')
const rmrf = require('rimraf')
const copy = require('ncp')
const colors = require('colors')
const _ = require('lodash')
const titleize = require('titleize')
const PDFDocument = require('pdfkit')
const utils = require('./utils.js')
const S = require('string')
const url = require('url')

// Set default options
program.version('0.1.1')
.option('-h, --hostname <hostname>', 'Set domain name.')
.option('-o, --output <output>', 'Set output folder.')
.option('-c, --count <count>', 'How many files are pdf will be generated.')
.option('-r, --rand <rand>', 'How many random keywords on index.')
.option('-k, --keyword <keyword>', 'Set keywords filename.')
.option('-g, --generate', 'Just generate (existing directory will not be removed)')
.parse(process.argv)

if (!program.hostname || !program.output) {
	return console.log('No hostname and/or output.')
}

if (!fs.existsSync(program.output)) {
	return console.log('Output is invalid folder.')
}

if (!program.count) {
	program.count = 0
}

// Preparation
copy.limit = 16

const uri = url.parse(program.hostname)
let templateDir = path.join(__dirname, 'templates')
let miscDir = path.join(templateDir, 'misc_template')
let configName = uri.hostname
if (uri.pathname) {
  let pathname = uri.pathname.split('/')
  pathname.pop()
  configName += pathname.join('.')
}
let outputDir = path.join(program.output, configName)
let sitemapDir = path.join(outputDir, 'sitemap')
let initialKeywords = null

const echo = (...args) => {
	let [sign, ...logs] = args
	let color = {
		'-': 'red',
		'+': 'blue',
		'OK': 'green',
		'i': 'yellow'
	}
	console.log(`[${sign}]`.bold[color[sign]], ...logs)
}


// First Step
// - Cleanup before create new hostname dir
const step1 = next => {
	echo('-', 'Remove dir:', outputDir)
	rmrf(outputDir, err => {
		if (err) {
			next(err)
		}

		echo('OK', outputDir, 'Removed')
		next()
	})
}

// Second step
// - Create hostname dir inside output dir
const step2 = next => {
	echo('+', 'Create dir:', outputDir)
	mkdir(outputDir, err => {
		if (err) {
			next(err)
		}

		echo('OK', outputDir, 'created')
		next()
	})
}

// Third step
// - Generate config based on host
// - Disable countdown
// - Disable auto subdomain
// - Set template as static
const step3 = next => {
	let defaultConfig = require(path.join(__dirname, 'config', 'default.json'))
	let config = utils.generateNewConfig(defaultConfig, configName)
	config.cpa.pageCountdown = false
	config.auto_subdomain = false
	config.static = true
	config.staticHost = program.hostname
	config.staticPathName = uri.pathname
	if (program.keyword) {
		config.keyword.filename = program.keyword
	}
	echo('OK', 'Setting config.')
	next(null, config)
}

// Fourth Step
// - Copy template from config folder to output
const step4 = (config, next) => {
	let htmlTemplate = path.join(templateDir, 'public', config.template)
	let destTemplate = path.join(outputDir, config.template)
	
	echo('i', 'Copy template:', htmlTemplate)
	copy(htmlTemplate, destTemplate, err => {
		if (err) {
			next(err)
		}

		echo('OK', htmlTemplate, 'copied')
		echo('-', 'Remove *.ejs', path.join(destTemplate, '*.ejs'))
		rmrf(path.join(destTemplate, '*.ejs'), err => {
			if (err) {
				next(err)
			}
			echo('OK', '*.ejs Removed')
			next(null, config)
		})
	})
}

// Fifth step
// - Read n keywords
let randKeyword = 0
const step5 = (config, next) => {
	echo('i', 'Read keywords')
	randKeyword = config.keyword.rand_size
	if (program.rand) {
		randKeyword = parseInt(program.rand)
	}
    utils.readKeyword(randKeyword, config.keyword.filename, (err, keywords) => {
        if (err) {
            next(err)
        }

        initialKeywords = keywords
		next(null, keywords, config)
    })
}

// Sixth Step
// - Generate index html with random keywords
// - Generate privacy.html
// - Generate dmca.html
// - Generate contact.html
// - Generate robots.txt
const step6 = (keywords, config, next) => {
	let indexTemplate = path.join(
		templateDir,
		'public',
		config.template,
		'index.ejs'
	)

	let indexHtml = ejs.render(fs.readFileSync(indexTemplate).toString(), {
		config,
		utils,
		keywords,
		host: program.hostname
	})

	fs.writeFileSync(path.join(outputDir, 'index.html'), indexHtml)
	echo('OK', 'Create index.html')

  let baseURL = program.hostname
  if (baseURL[baseURL.length - 1] === '/') {
    baseURL = baseURL.split('')
    baseURL.pop()
    baseURL = baseURL.join('')
  }
	let data = {
    config,
    utils,
    baseURL,
    year: new Date().getFullYear()
  }
    
    let privacyHtml = ejs.render(fs.readFileSync(path.join(miscDir, `privacy.ejs`)).toString(), data)
	fs.writeFileSync(path.join(outputDir, 'privacy.html'), privacyHtml)
	echo('OK', 'Create privacy.html')

    let dmcaHtml = ejs.render(fs.readFileSync(path.join(miscDir, `dmca.ejs`)).toString(), data)
	fs.writeFileSync(path.join(outputDir, 'dmca.html'), dmcaHtml)
	echo('OK', 'Create dmca.html')
	
    let contactHtml = ejs.render(fs.readFileSync(path.join(miscDir, `contact.ejs`)).toString(), data)
	fs.writeFileSync(path.join(outputDir, 'contact.html'), contactHtml)
	echo('OK', 'Create contact.html')

	let robots = ejs.render(fs.readFileSync(path.join(miscDir, `robots.ejs`)).toString(), {config})
	fs.writeFileSync(path.join(outputDir, 'robots.txt'), robots)
	echo('OK', 'Create robots.txt')

	next(null, config)
}

// 7th Step
// - Create sitemap dir
const step7 = (config, next) => {
	echo('+', 'Create sitemap dir:', sitemapDir)
	mkdir(sitemapDir, err => {
		if (err) {
			next(err)
		}

		echo('OK', sitemapDir, 'was created')
		next(null, config)
	})
}

// 8th Step
// - Create index-files.xml
// - Copy sitemap.xsl
const step8 = (config, next) => {
	echo('i', 'Create index-files.xml')

	let alphabet = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	let urlList = _.chain(alphabet)
	.lowerCase()
	.split('')
	.compact()
	.filter(data => data != ' ')
	.map(item => {
    const fullUrl = Object.assign({}, uri)
    fullUrl.pathname += `sitemap/file-${item}.xml`
    return url.format(fullUrl)
  })
	.value()

	echo('i', 'Create index-files.xml')
	let indexSitemap = ejs.render(fs.readFileSync(path.join(miscDir, `sitemap.ejs`)).toString(), {
		config,
		urls: urlList
	})
	fs.writeFileSync(path.join(outputDir, 'sitemap', 'index-files.xml'), indexSitemap)
	echo('OK', 'index-files.xml was created')

	echo('i', 'Copy sitemap.xsl')
	copy(path.join(templateDir, 'public', 'sitemap.xsl'), path.join(outputDir, 'sitemap.xsl'), err => {
		if (err) {
			next(err)
		}
		echo('OK', 'sitemap.xsl was copied')
		next(null, config)
	})
}

// 9th Step
// - Create view and download dir
const step9 = (config, next) => {
	let downloadDir = path.join(outputDir, config.permalink.view, config.permalink.download)
	echo('+', 'Create dir:', downloadDir)
	mkdir(downloadDir, err => {
		if (err) {
			next(err)
		}

		echo('OK', downloadDir, 'created')
		next(null, config)
	})
}

// 10th Step
// - Pick random keyword until program.count
const step10 = (config, next) => {
	echo('i', 'Generate keywords')
	let remaining = parseInt(program.count) - randKeyword
	utils.readKeyword(remaining, config.keyword.filename, (err, keywords) => {
		if (err) {
			next(err)
		}
		initialKeywords = initialKeywords.concat(keywords)
		next(null, config)
	})
}

// 11th step
// Convert all keywords to pdf
const concurrent = 5
const step11 = (config, next) => {
	echo('OK', `Save ${initialKeywords.length} files`)
	const downloadDir = path.join(outputDir, config.permalink.view, config.permalink.download)
	const templateDir = path.join(__dirname, 'templates')
	const downloadImg = path.join(templateDir, 'download_img')
	const pdfTemplate = require(path.join(templateDir, 'pdf_template'))
	const q = async.queue((task, callback) => {
		async.waterfall([
			(next) => {
				let slug = S(task.keyword).latinise().slugify().s
				echo('i', `Save ${slug}.pdf`)
				let templateName = utils.randomPDFTemplate(slug)
				let renderer = pdfTemplate[templateName]
				let subject = titleize(S(slug).humanize().s)
				let uniqueCode = utils.generateUniqueCode(subject, program.hostname, templateName)
				let title = `${subject} ${uniqueCode}`.toUpperCase()
				let fakeAuthor = utils.generateFakeName(subject)
				let data = {
					config,
					host: program.hostname,
					templateName,
					utils,
					baseURL: program.hostname,
					title,
					downloadImg
				}

				let doc = new PDFDocument({
			        size: 'A4',
			        margin: 50,
			        info: {
			            Title: title,
			            Author: fakeAuthor,
			            Subject: subject,
			            Keywords: ''
			        }
			    })
				data.doc = doc
				renderer(data, fs.createWriteStream(path.join(downloadDir, `${slug}.pdf`)))
				echo('OK', `${slug}.pdf was saved`)
				next()
			},

			(next) => {
				if (task.index % 10 === 0) {
					echo('i', `Current index ${task.index} of ${initialKeywords.length}`)
				}
				next()
			}
		], () => {
			setTimeout(() => {
				callback()
			}, 10000)
		})
	}, concurrent)

	q.drain = () => {
		next(null, config)
	}

	for (let i = 0; i<initialKeywords.length; i++) {
		q.push({
			keyword: initialKeywords[i],
			index: i
		}, err => {
			if (err) {
				echo('-', err)
			}
		})
	}
}


// 12th step
// Convert existing pdf to sitemap
const step12 = (config, next) => {
	const downloadDir = path.join(outputDir, config.permalink.view, config.permalink.download)
	const downloadPath = `${config.permalink.view}/${config.permalink.download}/`
	const queue = async.queue((task, callback) => {
		let urls = task.keywords.map(file => {
      const fullUrl = Object.assign({}, uri)
      fullUrl.pathname += downloadPath + file
      return url.format(fullUrl)
		})
		let sitemapTemplate = ejs.render(fs.readFileSync(path.join(miscDir, `sitemap.ejs`)).toString(), {
			config,
			urls
		})

		let sitemapName = `file-${task.alphabet}.xml`
		echo('i', `Create ${sitemapName}`)
		fs.writeFile(path.join(sitemapDir, sitemapName), sitemapTemplate, err => {
			if (err) {
				next(err)
			}

			echo('OK', `${sitemapName} was created`)
			callback()
		})
	})

	queue.drain = () => {
		next(null, config)
	}

	let files = fs.readdirSync(downloadDir)
	let alphabet = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.toLowerCase().split('')
	_.each(alphabet, (alpha, index) => {
		let keywords = files.filter(function(word) {
			return word[0] === alpha
		})

		queue.push({
			alphabet: alpha,
			keywords: keywords
		})
	})
}

// Okay let's begin
// Run all steps.
let steps = []

if (!program.generate) {
	steps = [
		step1, step2, step3,
		step4, step5, step6,
		step7, step8, step9,
		step10, step11, step12
	]
} else {
	steps = [step3, step5, step6, step10, step11, step12]
}

async.waterfall(steps, (err, done) => {
	if (err) {
		echo('-', err)
		return
	}
	console.log('DONE')
})