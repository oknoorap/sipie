const path = require('path')
const fs = require('fs')
const faker = require('faker')
const LineByLineReader = require('line-by-line')
const _ = require('lodash')
const S = require('string')
const titleize = require('titleize')
const moment = require('moment')
const filesize = require('filesize')
const url = require('url')
const urlencode = require('urlencode')
let templateDir = path.join(__dirname, 'templates')
    // convert number to string
const getNumberFromString = s => {
        var a
        return s.split("").reduce((a, b) => {
            a = ((a << 5) - a) + b.charCodeAt(0)
            return a & a
        }, 0)
    }
    // read keywords directory
const keywordFiles = () => {
        let files = fs.readdirSync('./keywords')
        return files
    }
    // pick random keyword by seed
const pickKeyword = domainName => {
    let files = keywordFiles().filter(f => f.indexOf('.txt')>0)
    let seedNumber = getNumberFromString(domainName)
    faker.seed(seedNumber)
    return faker.random.arrayElement(files)
}
const pickPDFTemplate = slug => {
        let files = _.pull(fs.readdirSync(path.join(templateDir, 'pdf_template')), 'index.js').filter(f => f.indexOf('.otf')<0)
        let seedNumber = getNumberFromString(slug)
        faker.seed(seedNumber)
        return faker.random.arrayElement(files).replace(/\.js$/ig, '')
    }
    // templates name list
const listTemplate = () => {
        let files = fs.readdirSync(path.join(templateDir, 'public'))
        files = _.pull(files, 'sitemap.xsl')
        return files
    }
    // pick random template by seed
const pickTemplate = domainName => {
        let files = listTemplate()
        let seedNumber = getNumberFromString(domainName)
        faker.seed(seedNumber)
        return faker.random.arrayElement(files)
    }
    // random value from spinner
const randomSpinner = (domainName, values) => {
    let seedNumber = getNumberFromString(domainName)
    faker.seed(seedNumber)
    return faker.random.arrayElement(values)
}
module.exports = {
    generateNewConfig (config, domainName) {
        config.name = this.generateFakeName(domainName)
            
        // set keyword file
        config.keyword.filename = pickKeyword(domainName)
        
        // pick template
        config.template = pickTemplate(domainName)
         
        // from spinner
        config.title = randomSpinner(domainName, config.spinner.title)
        config.permalink.view = randomSpinner(domainName, config.spinner["permalink.view"])
        config.permalink.download = randomSpinner(domainName, config.spinner["permalink.download"])
        
        // delete spinner
        delete config.spinner
        
        // add sitemap_xml last_mod
        config.sitemap_xml.last_mod = this.randomDateUntilNow('YYYY-MM-DD HH:mm')
        return config
    },

    domainConfig(domainName) {
        if (domainName.substr(0, 4) === 'www.') {
            domainName = domainName.substr(4, domainName.length)
        }

        let defaultConfigPath = path.join(__dirname, 'config', 'default.json')
        let configPath = path.join(__dirname, 'config', `${domainName}.json`)
        
        if (!fs.existsSync(configPath)) {
            let config = fs.readFileSync(defaultConfigPath, 'utf8')
            config = JSON.parse(config);
            config = this.generateNewConfig(config, domainName)

            fs.writeFileSync(configPath, JSON.stringify(config, null, 2))
            return config
        } else {
            return require(configPath)
        }
    },
    // read keyword
    readKeyword(size, filename, callback) {
        const readBigFile = new LineByLineReader('./keywords/' + filename)
        let keywords = []
        readBigFile.on('error', (err) => callback(err, null))
        readBigFile.on('line', (line) => {
            keywords.push(line)
        })
        readBigFile.on('end', () => {
            let randKeywords = _.sampleSize(keywords, size)
            callback(null, randKeywords)
        })
    },
    permalinkInSitemap(config, alphabet, title, ext) {
        let basename = S(title).slugify().s
        return `/${config.permalink.view}/${alphabet}/${basename}${ext}`
    },
    permalink(config, title) {
        const staticHost = config.static ? config.staticHost : '/'
        let basename = S(title).slugify().s
        return `${staticHost}${config.permalink.view}/${config.permalink.download}/${basename}`
    },
    sitemapPermalink(dirname, prefix, v, ext) {
        v = v.toLowerCase()
        return `/${dirname}/${prefix}-${v}.${ext}`
    },
    randomSubdomain(config, host, pattern) {
        if (! config.auto_subdomain || host.split('.').length>3) {
            return
        }
            
        if (typeof pattern === 'undefined') {
            let randomVLen = _.random(1, 7)
            let randomCLen = _.random(1, 20)
            let randomSampleSize = _.random(4, 15)
            let v = _.repeat('v', randomVLen)
            let c = _.repeat('c', randomCLen)
            let concat = _.concat(v.split(''), c.split(''))
            let sampleSize = _.sampleSize(concat, randomSampleSize)
            pattern = sampleSize.join('')
        }
        let possibleC = "BCDFGHJKLMNPQRSTVWXZ";
        let possibleV = "AEIOUY";
        const randomCharacter = bucket => bucket.charAt(Math.floor(Math.random() * bucket.length))
        let pIndex = pattern.length;
        let res = new Array(pIndex);
        while (pIndex--) {
            res[pIndex] = pattern[pIndex].replace(/v/, randomCharacter(possibleV)).replace(/c/, randomCharacter(possibleC));
        }
        let subdomain = res.join("").toLowerCase()
        return `<a href="http://${subdomain}.${host}">${subdomain}.${host}</a> | `
    },
    sitemapFooter(prefix, config) {
      let alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789".split("")
      let results = []
      alphanumeric.forEach(a => {
        let attrTitle = `Sitemap ${a}`
        let permalink = this.sitemapPermalink('sitemap', prefix, a, 'xml')
        if (config.static) {
          const fullPermalink = url.parse(config.staticHost)
          fullPermalink.pathname += `sitemap/${prefix}-${a.toLowerCase()}.xml`
          permalink = url.format(fullPermalink)
        }
        results.push(`<a href="${permalink}" title="${attrTitle}" alt="${attrTitle}">${a}</a>`)
      })
      return results.join(" ")
    },
    showRandomKeywords(config, keywords) {
      if (config.keyword.rand_size > 0) {
        let results = []
        keywords.forEach(k => {
          results.push(`<a href="${this.permalink(config, k)}.pdf">${titleize(k)}</a>`)
        })
        return results.join(' | ')
      }
    },
    randomPDFTemplate(slug) {
        return pickPDFTemplate(slug)
    },
    sitemapFooterBlock(config) {
        let sitemapFooterPage = this.sitemapFooter('page', config)
        let sitemapFooterFile = this.sitemapFooter('file', config)
        let staticHost = config.static ? config.staticHost : '/'
        let retval = `<a href="${staticHost}privacy.html">Privacy</a> | <a href="${staticHost}dmca.html">DMCA</a> | <a href="${staticHost}contact.html">Contact</a><br />`

        if (!config.static) {
            retval += `<a href="/sitemap/index-pages.xml">Pages</a>${sitemapFooterPage}<br />`
        }

        retval += `<a href="${staticHost}sitemap/index-files.xml">Files</a>${sitemapFooterFile}`

        return retval
    },
    histats(config) {
        return `<!-- Histats.com  START  (aync)-->
    <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.start', '1,${config.histats},4,0,0,0,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('http://s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();</script>
    <noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?${config.histats}&101" alt="Website Statistics" border="0"></a></noscript>
    <!-- Histats.com  END  -->`
    },
    randomDateUntilNow(format) {
        let firstJan = `01-01-${moment().format('YYYY')}`
        let startDate = moment(firstJan, 'MM-DD-YYYY').format('X')
        let endDate = moment().format('X')
        let randDate = _.random(startDate, endDate)
        return moment.unix(randDate).format(format)
    },
    listKeywordByAlphabet(alphabet, filename, callback) {
        const readBigFile = new LineByLineReader('./keywords/' + filename)
        let keywords = []
        readBigFile.on('error', (err) => callback(err, null))
        readBigFile.on('line', (line) => {
            let pattern = new RegExp(`^${alphabet}`, 'ig')
            if (line.match(pattern)) {
                keywords.push(line)
            }
        })
        readBigFile.on('end', () => {
            callback(null, keywords)
        })
    },
    // random pdf page size
    randomPDFTotalPage(slug, pageLabel) {
        let seedNumber = getNumberFromString(slug)
            // generate seed
        faker.seed(seedNumber)
            // get random number between 20 to 50
        let randNumber = faker.random.number({
            min: 20,
            max: 50
        })
        return `${randNumber} ${pageLabel}`
    },
    randomPDFSize(slug) {
        let seedNumber = getNumberFromString(slug)
            // generate seed
        faker.seed(seedNumber)
            // get random number between 20 to 50
        let randNumber = faker.random.number({
            min: 600,
            max: 2048
        })
        return filesize(randNumber * 1024, {
            base: 10
        })
    },
    generateUniqueCode(title, hostname, pageTemplate) {
        let strArray = _.chain(title).split(' ').value()
        let shortName = ''
        _.each(strArray, str => {
            shortName += str.substr(0, 1)
        })
        let uniqueDomain = hostname.substr(0, 2) + hostname.substr(-2)
        uniqueDomain = uniqueDomain.toUpperCase()
        let uniqueCode
        switch (pageTemplate) {
            case "a":
                uniqueCode = shortName.toUpperCase() + parseInt(Math.round(title.length / 2))
                uniqueCode += `-${uniqueDomain + title.length%15}`
                break;
            case "b":
                uniqueCode = shortName.toUpperCase() + parseInt(Math.round(title.length / 2))
                uniqueCode += `${uniqueDomain}-${title.length % 15}`
                break;
            case "c":
                uniqueCode = `PDF-${shortName.toUpperCase()}${parseInt(Math.round(title.length / 3))}-${uniqueDomain}${title.length%12}`
                break;
            case "d":
                uniqueCode = `${shortName.toUpperCase()}PDF-${uniqueDomain}${parseInt(Math.round(title.length /3))}${title.length%12}`
                break;
            case "e":
                uniqueCode = `PDF-${parseInt(Math.round(title.length / 4))}${shortName.toUpperCase()}${title.length%19}${uniqueDomain}`
                break;
            case "f":
                uniqueCode = `${uniqueDomain + parseInt(Math.round(title.length / 2) + title.length / 11)}-PDF${shortName.toUpperCase()}`
                break;
            case "g":
                uniqueCode = `${shortName.toUpperCase()}-${parseInt(Math.round(title.length / 3))}${uniqueDomain}${title.length % 13}-PDF`
                break;
            case "h":
                uniqueCode = `PDF-${uniqueDomain}${shortName.toUpperCase()}-${parseInt(Math.round(title.length /3))}-${title.length % 13}`
                break;
            case "i":
                uniqueCode = `${shortName.toUpperCase()}PDF-${parseInt(Math.round(title.length / 2))}-${title.length %12}`
                break;
            case "j":
                uniqueCode = `${uniqueDomain}${parseInt(Math.round(title.length / 4))}${title.length % 13}-PDF-${shortName.toUpperCase()}`
                    //$kode_unik = $domain_unik.(round(strlen($title)/4)).(strlen($title)%13)."-PDF-".strtoupper($singkatan);
                break;
            case "k":
                uniqueCode = `${uniqueDomain}-${parseInt(Math.round(title.length / 3))}-${shortName.toUpperCase()}${title.length %12}`
                    // $kode_unik = $domain_unik."-".(round(strlen($title)/3))."-".strtoupper($singkatan).(strlen($title)%12);
                break;
            case "l":
                uniqueCode = `PDF-${uniqueDomain}${parseInt(Math.round(title.length / 5))}-${shortName.toUpperCase()}-${title.length % 11}`
                    //$kode_unik = "PDF-".$domain_unik.(round(strlen($title)/5))."-".strtoupper($singkatan)."-".(strlen($title)%11);
                break;
            case "m":
                uniqueCode = `${uniqueDomain}${parseInt(Math.round(title.length /5))}-${shortName.toUpperCase()}PDF-${title.length % 11}`
                    // $kode_unik = $domain_unik.(round(strlen($title)/5))."-".strtoupper($singkatan)."PDF-".(strlen($title)%11);
                break;
            case "n":
                uniqueCode = `PDF-${uniqueDomain}${parseInt(Math.round(title.length / 7))}${shortName}${title.length % 22}`
                    // $kode_unik = "PDF-".$domain_unik.(round(strlen($title)/7)).strtoupper($singkatan).(strlen($title)%22);
                break;
            case "o":
                uniqueCode = `${uniqueDomain}${title.length % 17}-PDF-${shortName.toUpperCase()}${parseInt(Math.round(title.length / 4))}`
                    // $kode_unik = $domain_unik.(strlen($title)%17)."-PDF-".strtoupper($singkatan).(round(strlen($title)/4));
                break;
            case "p":
                uniqueCode = `${shortName.toUpperCase()}-${parseInt(Math.round(title.length / 2))}-${uniqueDomain}${title.length % 15}-PDF`
                    // $kode_unik = strtoupper($singkatan)."-".(round(strlen($title)/2))."-".$domain_unik.(strlen($title)%15)."-PDF";
                break;
            case "q":
                uniqueCode = `${uniqueDomain}-PDF-${shortName.toUpperCase()}-${parseInt(Math.round(title.length /5))}-${title.length %11}`
                    //$kode_unik = $domain_unik."-PDF-".strtoupper($singkatan)."-".(round(strlen($title)/5))."-".(strlen($title)%11);
                break;
            case "r":
                uniqueCode = `${shortName.toUpperCase()}${uniqueDomain}-PDF${parseInt(Math.round(title.length / 3))}-${title.length%14}`
                    // $kode_unik = strtoupper($singkatan).$domain_unik."-PDF".(round(strlen($title)/3))."-".(strlen($title)%14);
                break;
            case "s":
                uniqueCode = `${uniqueDomain}-${parseInt(Math.round(title.length / 5))}${title.length %9}PDF-${shortName.toUpperCase()}`
                    /// $kode_unik = $domain_unik."-".(round(strlen($title)/5)).(strlen($title)%9)."PDF-".strtoupper($singkatan);
                break;
            case "t":
                uniqueCode = `PDF-${shortName.toUpperCase()}-${parseInt(Math.round(title.length/4))}${uniqueDomain}-${title.length % 17}`
                    // $kode_unik = "PDF-".strtoupper($singkatan)."-".(round(strlen($title)/4)).$domain_unik."-".(strlen($title)%17);
                break;
        }
        return uniqueCode
    },
    // create fake name
    generateFakeName(domainName) {
        let seedNumber = getNumberFromString(domainName)
        faker.seed(seedNumber)
        return faker.name.findName()
    },
    generateRandomFileCreationDate(title, format = 'D MMM, YYYY') {
        let seedNumber = getNumberFromString(title)
            // create random creation date
        faker.seed(seedNumber)
        let creationDate = faker.date.between(new Date('6/7/2004'), new Date('8/8/2016'));
        return moment(creationDate).format(format)
    },
    baseURL(req) {
        let {
            hostname,
            protocol
        } = req
        let urlObj = {
            protocol,
            hostname
        }
        let port = req.app.get('port')
        if (port) {
            urlObj.port = port
        }

        let retval = url.format(urlObj).trim()
        return retval
    },
    variationsTitle(config, title) {
        let originalTitle = title.replace(/\s{2,}/gi, ' ')
        let re = new RegExp(`\\b(${config.variations.join('|')})\\b\\s{0,}$`, 'gi')
        while (re.test(title)) {
            title = title.replace(re, '')
        }
        let variant = _.chain(config.variations).map(data => `${title} ${data}`).map(data => data.replace(/\s{2,}/gi, ' ')).uniq().filter(data => {
            return data.toLowerCase() !== originalTitle.toLowerCase()
        }).value()
        return variant
    },
    parseLandingURL(config, s, title) {
        return s.replace(/\$prod_id/ig, config.cpa.prod_id).replace(/\$ref/ig, config.cpa.ref).replace(/\$title/ig, urlencode(title))
    },
    landingPageURL(baseURL, config, title) {
        if (config.cpa.pageCountdown) {
            let slug = S(title).slugify().s
            return `${baseURL}/${config.permalink.download}/${slug}.pdf`
        }
        if (_.isArray(config.cpa.landing_url)) {
            let seedNumber = getNumberFromString(title)
            faker.seed(seedNumber)
            let landingPage = faker.random.arrayElement(config.cpa.landing_url)
            return this.parseLandingURL(config, landingPage, title)
        }
        let singleLandingPageURL = config.cpa.landing_url
        return this.parseLandingURL(config, singleLandingPageURL, title)
    },

    /**
     * Capitalize string
     */
    capitalize (str) {
        return str.replace(/\w\S*/g, txt => {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
        })
    }
}
